#!/usr/bin/env bash
# Run script for docker compose v2
docker network create nginx-proxy
wget https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl
docker compose up -d
