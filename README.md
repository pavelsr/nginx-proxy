Repository for fast setup and inspect of [nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)

Using docker-compose and following images as base:

```
nginx
jwilder/docker-gen
jrcs/letsencrypt-nginx-proxy-companion
```

# Running

```
./run.sh
```

# Tesing

By default, if you will query `http://localhost` after running nginx-proxy it will print 503 Service Temporarily Unavailable

So you need to run external container to check that nginx-proxy is running fine

Variant 1 (special whoami image, ~10MB):

```
docker run -d -e VIRTUAL_HOST=foo.bar.com -e VIRTUAL_PORT=8000 --network=nginx-proxy jwilder/whoami
curl -H "Host: foo.bar.com" localhost  # will print contaner id
```

Variant 2 (any web server, ~50MB):

```
docker run -d -e VIRTUAL_HOST=a.local.tst --network=nginx-proxy --name simplehttpserver httpd:alpine
curl -H "Host: a.local.tst" localhost  # # will print HTML with "It works!" page
```

(you can also use any simple http server with non-empty /var/www )

If you want to locally test with browser you also need to make a new record about VIRTUAL_HOST in /etc/hosts (simplest way to setup DNS forwaring to the host running nginx-proxy)

# Stopping

```
docker-compose down
```

# Logging

all:

```
docker-compose logs -f -n 15
```

troubleshoting https sites:

```
docker logs -f --tail 15 letsencrypt-nginx-proxy-companion
```
